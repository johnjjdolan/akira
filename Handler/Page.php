<?php

namespace Akira\Handler;

/**
 *
 */
class Page extends BaseHandler
{
    public function __construct()
    {
        $this->entity = new \Akira\Entity\Page();
    }

    /**
     * @param array $data
     * @return array
     */
    public function validate(array $data = []): array
    {
        $errors = [];
        if (empty($data['name'])) {
            $errors[] = '(!) Error: You must provide a name';
        }

        return $errors;
    }
}