<?php

namespace Akira\Handler;

use Akira\Entity\BaseEntity;

/**
 *
 */
class BaseHandler
{
    /** @var BaseEntity */
    protected BaseEntity $entity;

    /**
     * @param array $data
     * @return array
     */
    public function getAll(array $data = []): array
    {
        $items = $this->entity->get($data);
        $output = [];
        foreach($items as $item) {
            $output[] = new $this->entity($item);
        }
        return $output;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function getOne(array $data = [])
    {
        $data['limit'] = 1;
        return current($this->getAll($data));
    }

    /**
     * Invoke the DB save
     *
     * @param array $data
     * @return false|string
     */
    private function save(array $data = [])
    {
        return $this->entity->save($data);
    }

    /**
     * Create a new record
     *
     * @param array $data
     * @return false|string
     */
    public function add(array $data = [])
    {
        return $this->save($data);
    }

    /**
     * Update the record
     *
     * @param array $data
     * @return false|string
     */
    public function update(array $data = [])
    {
        return $this->save($data);
    }

    /**
     * Delete a new record
     *
     * @param array $data
     * @return bool|false
     */
    public function delete(array $data = []): bool
    {
        return $this->entity->delete($data);
    }

    /**
     * @param array $data
     * @return array
     */
    public function validate(array $data = []): array
    {
        return [];
    }
}