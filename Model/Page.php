<?php

namespace Akira\Model;

/**
 * Page Model
 */
class Page extends BaseModel
{
    /**
     * @param array $data
     * @return false|string
     */
    public function save(array $data = []): bool
    {
        // invoke the save on the entity
        $entity = new \Akira\Entity\Page();
        return $data['id'] ? $entity->update($data) : $entity->insert($data);
    }

    /**
     * @param array $data
     * @return false|string
     */
    public function delete(array $data = []): bool
    {
        // invoke the save on the entity
        $entity = new \Akira\Entity\Page();
        return $entity->delete($data);
    }
}