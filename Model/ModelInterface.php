<?php

namespace Akira\Model;

/**
 * Model Interface
 */
interface ModelInterface
{
    /**
     * @param array $data
     * @return bool
     */
    public function save(array $data = []): bool;

    /**
     * @param array $data
     * @return bool
     */
    public function delete(array $data = []): bool;
}