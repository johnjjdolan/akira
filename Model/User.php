<?php

namespace Akira\Model;

/**
 * User Model
 */
class User extends BaseModel
{
    /**
     * @param array $data
     * @return false|string
     */
    public function save(array $data = []): bool
    {
        // invoke the save on the entity
        $entity = new \Akira\Entity\User();

        // handle the password encrypt
        $hashPassword = empty($data['id']);
        if ($data['id']) {
            $user = (new \Akira\Handler\User())->getOne(['id' => $data['id']]);
            if (!empty($data['password']) && $data['password'] !== $user->getPassword()) {
                $hashPassword = true;
            }
        }
        if ($hashPassword) {
            $data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
        }
        if (isset($data['password']) && empty($data['password'])) {
            unset($data['password']);
        }

        return $data['id'] ? $entity->update($data) : $entity->insert($data);
    }

    /**
     * @param array $data
     * @return false|string
     */
    public function delete(array $data = []): bool
    {
        // invoke the save on the entity
        $entity = new \Akira\Entity\User();
        return $entity->delete($data);
    }
}