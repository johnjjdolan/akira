<?php

namespace Akira\Model;

/**
 * Base Model
 */
class BaseModel implements ModelInterface
{
    /**
     * @param array $data
     * @return bool
     */
    public function save(array $data = []): bool
    {
        return false;
    }

    /**
     * @param array $data
     * @return bool
     */
    public function delete(array $data = []): bool
    {
        return false;
    }
}