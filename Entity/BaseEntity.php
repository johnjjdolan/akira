<?php

namespace Akira\Entity;

use Akira\Core\Database;
use Akira\Core\Site;

/**
 * Base Entity
 */
class BaseEntity extends Database
{
    /** @var string  */
    protected string $table = '';
    /** @var string */
    protected string $type = 'BaseEntity';

    /**
     * Fetch content from the entity table
     *
     * @param array $data
     * @return array|false
     */
    public function get(array $data = [])
    {
        $tableFields = $this->lookupTable($this->table);
        $where = [];
        $parameters = [];
        $order = null;
        $group = null;

        // always respect the site id
        $data['site_id'] = (new Site())->getSiteId();

        // Process args
        foreach ($data as $field => $parameter) {
            // Ignore pagination, ordering, grouping, centre_id
            if (in_array($field, array('order_by', 'group_by', 'start', 'per_page', 'centre_id'))) {
                continue;
            }

            // Are we looking for null?
            if (is_null($parameter)) {
                $where[] = "$tableFields[$field] IS NULL";
                continue;
            }

            // Are we searching?
            if (substr($field, 0, 5) == 'srch_') {
                $field = substr($field, 5);
                $where[] = "AND $tableFields[$field] LIKE ?";
                $parameters[] = '%' . $parameter . '%';
                continue;
            }

            // Are we looking for in/not in?
            if (substr($field, -3) == '_in' && is_array($parameter)) {
                if (substr($field, -7) == '_not_in') {
                    $field = substr($field, 0, -7);
                    $where[] = "$tableFields[$field] NOT IN (";
                } else {
                    $field = substr($field, 0, -3);
                    $where[] = "$tableFields[$field] IN (";
                }

                $items = [];
                foreach ($parameter as $id) {
                    $items[] = '?';
                    $parameters[] = $id;
                }

                $where[] = implode(',', $items) . ')';
                continue;
            }

            // skip empty columns
            if (empty($tableFields[$field])) {
                continue;
            }

            // Normal select
            $where[] = "$tableFields[$field] = ?";
            $parameters[] = $parameter;
        }

        // Ordering
        if (array_key_exists('order_by', $data) && $data['order_by']) {
            $order = 'ORDER BY ' . prepForStore($data['order_by']);
        }

        // Grouping
        if (array_key_exists('group_by', $data) && $data['group_by']) {
            $group = 'GROUP BY ' . prepForStore($data['group_by']);
        }

        // combine SQL
        $sql = trim("SELECT * FROM " . $this->table . " " .
            ($where ? ('WHERE ' . implode("AND ", $where)) : " ") . " " .
            ($order ?? "") . " " . ($group ?? ""));

        // commit
        $stmt = ($this->connect())->prepare($sql);
        $stmt->execute($parameters);
        return $stmt->fetchAll();
    }

    /**
     * Fetch the table definition from JSON
     *
     * @param string $table
     * @return array
     */
    private function lookupTable(string $table = ''): array
    {
        if (empty($table)) {
            return [];
        }
        //TODO: Think of a better way to reference these JSON files
        return json_decode(file_get_contents('/var/akira/Core/Tables/' . $table . '.json'), 1);
    }

    /**
     * Add
     *
     * @param array $data
     * @return false|string
     */
    public function insert(array $data = [])
    {
        //$this->validate();

        // always respect the site id
        $data['site_id'] = (new Site())->getSiteId();

        $sql = "INSERT INTO {$this->table}";

        // fetch this table definition from JSON
        $tableFields = $this->lookupTable($this->table);

        // Placeholders for binding information, always bind insert to centre id by default
        $fields = [];
        $columns = [];

        // Add each arg passed
        foreach ($data as $field => $parameter) {
            // Ignore nulls and site_id
            if (is_null($parameter) || $field == 'id' || empty($tableFields[$field])) {
                continue;
            }

            $columns[] = $tableFields[$field];
            $parameters[] = $parameter;
            $fields[] = '?';
        }

        // Set created & updated at stamp
        if (!isset($parameters['created'])) {
            $columns[] = $tableFields['created'];
            $parameters[] = date('Y-m-d H:i:s');
            $fields[] = '?';
        }
        if (!isset($parameters['updated'])) {
            $columns[] = $tableFields['updated'];
            $parameters[] = date('Y-m-d H:i:s');
            $fields[] = '?';
        }
        $sql .= '(' . implode(',', $columns) . ') VALUES (' . implode(',', $fields) . ')';

        $connection = $this->connect();
        $connection->prepare($sql)->execute($parameters);
        return $connection->lastInsertId();
    }

    /**
     * Update
     *
     * @param  array $data
     * @return boolean
     */
    function update(array $data = [])
    {
        // always respect the site id
        $data['site_id'] = (new Site())->getSiteId();

        // fetch this table definition from JSON
        $tableFields = $this->lookupTable($this->table);

        $sql = "UPDATE $this->table SET ";

        // Placeholders for binding information, always bind insert to centre id by default
        foreach ($data as $field => $parameter) {
            // Ignore nulls and site_id
            if (is_null($parameter) || $field == 'id' || empty($tableFields[$field])) {
                continue;
            }
            $parameters[] = $parameter;
            $sql .= $tableFields[$field] . " = ? ,";
        }

        // update timestamp
        if (!isset($data['updated'])) {
            $sql .= $tableFields['updated'] . " = ? ";
            $parameters[] = date('Y-m-d H:i:s');
        }

        // add where
        $sql .= " WHERE {$tableFields['site_id']} = ? AND {$tableFields['id']} = ?";
        $parameters[] = $data['site_id'];
        $parameters[] = (int)$data['id'];

        $connection = $this->connect();
        return $connection->prepare($sql)->execute($parameters);
    }


    /**
     * Delete a item from the entity table
     *
     * @param array $data
     * @return bool|false
     */
    public function delete(array $data = []): bool
    {
        $id = $data['item_id'] ?? null;
        // always respect the site id
        $siteId = (new Site())->getSiteId();
        if (empty($id)) {
            return false;
        }
        $sql = "DELETE FROM {$this->table} WHERE id = ? AND site_id = ?";

        // remove
        return ($this->connect())->prepare($sql)->execute([$id, $siteId]);
    }

    /**
     * @return string
     */
    public function getTable(): string
    {
        return $this->table;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param $name
     * @param string $type
     * @param bool $required
     * @param bool $password
     * @return array[]
     */
    protected function outputField($name, string $type = 'string', bool $required = false, bool $password = false): array
    {
        return [
            'name' => $name,
            'type' => $type,
            'required' => $required,
            'password' => $password
        ];
    }

    /**
     * @param $field
     * @return null
     */
    public function getValue($field)
    {
        if (property_exists($this, $field)) {
            return $this->$field;
        }
        return null;
    }
}