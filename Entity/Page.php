<?php

namespace Akira\Entity;

/**
 * Page Entity
 */
class Page extends BaseEntity
{
    /** @var string */
    protected string $table = 'pages';
    /** @var string */
    protected string $type = 'Page';

    /** @var int|null */
    protected ?int $id;
    /** @var string|null */
    protected ?string $name;
    /** @var string|null */
    protected ?string $slug;
    /** @var string|null */
    protected ?string $template;
    /** @var string|null */
    protected ?string $content;
    /** @var int|null */
    protected ?int $site_id;
    /** @var string|null */
    protected ?string $created;
    /** @var string|null */
    protected ?string $updated;

    /**
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->setId($data['id'] ?? null);
        $this->setName($data['name'] ?? null);
        $this->setSlug($data['slug'] ?? null);
        $this->setTemplate($data['template'] ?? null);
        $this->setContent($data['content'] ?? null);
        $this->setCreated($data['created'] ?? null);
        $this->setSiteId($data['site_id'] ?? null);
        $this->setCreated($data['created'] ?? null);
        $this->setUpdated($data['updated'] ?? null);
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Page
     */
    public function setId(?int $id): Page
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Page
     */
    public function setName($name): Page
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getSiteId(): ?int
    {
        return $this->site_id;
    }

    /**
     * @param int|null $site_id
     * @return Page
     */
    public function setSiteId(?int $site_id): Page
    {
        $this->site_id = $site_id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCreated(): ?string
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     * @return Page
     */
    public function setCreated($created): Page
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUpdated(): ?string
    {
        return $this->updated;
    }

    /**
     * @param mixed $updated
     * @return Page
     */
    public function setUpdated($updated): Page
    {
        $this->updated = $updated;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @param string|null $slug
     * @return Page
     */
    public function setSlug(?string $slug): Page
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string|null $content
     * @return Page
     */
    public function setContent(?string $content): Page
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTemplate(): ?string
    {
        return $this->template;
    }

    /**
     * @param string|null $template
     * @return Page
     */
    public function setTemplate(?string $template): Page
    {
        $this->template = $template;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getFields(): array
    {
        return [
            $this->outputField('name', 'string', true),
            $this->outputField('slug'),
            $this->outputField('template', 'string', true),
            $this->outputField('content', 'textarea')
        ];
    }
}