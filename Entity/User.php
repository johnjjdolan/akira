<?php

namespace Akira\Entity;

/**
 *
 */
class User extends BaseEntity
{
    /** @var string */
    protected string $table = 'users';
    /** @var string */
    protected string $type = 'User';

    /** @var int|null  */
    protected ?int $id;
    /** @var string|null  */
    protected ?string $name;
    /** @var int|null  */
    protected ?int $site_id;
    /** @var string|null  */
    protected ?string $firstname;
    /** @var string|null  */
    protected ?string $surname;
    /** @var string|null  */
    protected ?string $email;
    /** @var string|null */
    protected ?string $login;
    /** @var string|null  */
    protected ?string $password;
    /** @var string|null  */
    protected ?string $created;
    /** @var string|null  */
    protected ?string $updated;

    /**
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        // TODO: AutoSet Data
        $this->setId($data['id'] ?? null);
        $this->setName($data['name'] ?? null);
        $this->setSiteId($data['site_id'] ?? null);
        $this->setFirstname($data['firstname'] ?? null);
        $this->setSurname($data['surname'] ?? null);
        $this->setEmail($data['email'] ?? null);
        $this->setLogin($data['login'] ?? null);
        $this->setPassword($data['password'] ?? null);
        $this->setCreated($data['created'] ?? null);
        $this->setUpdated($data['updated'] ?? null);
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return User
     */
    public function setId($id): User
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return User
     */
    public function setName($name): User
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getSiteId(): ?int
    {
        return $this->site_id;
    }

    /**
     * @param mixed $site_id
     * @return User
     */
    public function setSiteId($site_id): User
    {
        $this->site_id = $site_id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     * @return User
     */
    public function setFirstname($firstname): User
    {
        $this->firstname = $firstname;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSurname(): ?string
    {
        return $this->surname;
    }

    /**
     * @param mixed $surname
     * @return User
     */
    public function setSurname($surname): User
    {
        $this->surname = $surname;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return User
     */
    public function setEmail($email): User
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLogin(): ?string
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     * @return User
     */
    public function setLogin($login): User
    {
        $this->login = $login;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     * @return User
     */
    public function setPassword($password): User
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCreated(): ?string
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     * @return User
     */
    public function setCreated($created): User
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUpdated(): ?string
    {
        return $this->updated;
    }

    /**
     * @param mixed $updated
     * @return User
     */
    public function setUpdated($updated): User
    {
        $this->updated = $updated;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string[]
     */
    public function getFields(): array
    {
        return [
            $this->outputField('email', 'string', true),
            $this->outputField('firstname'),
            $this->outputField('surname'),
            $this->outputField('login', 'string', true),
            $this->outputField('password', 'string', false, true),
        ];
    }
}