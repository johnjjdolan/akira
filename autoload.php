<?php

// template engine
require_once('/var/lib/smarty/Smarty.class.php');

require_once 'Core/Database.php';
require_once 'Core/Authenticate.php';
require_once 'Core/Response.php';
require_once 'Core/Renderer.php';
require_once 'Core/Routing/Route.php';
require_once 'Core/Site.php';
require_once 'Entity/BaseEntity.php';
require_once 'Entity/Page.php';
require_once 'Entity/User.php';
require_once 'Controllers/ControllerInterface.php';
require_once 'Controllers/DefaultController.php';
require_once 'Controllers/User.php';
require_once 'Controllers/Page.php';
require_once 'Controllers/Authorise.php';
require_once 'Handler/BaseHandler.php';
require_once 'Handler/Page.php';
require_once 'Handler/User.php';
require_once 'Model/ModelInterface.php';
require_once 'Model/BaseModel.php';
require_once 'Model/Page.php';
require_once 'Model/User.php';
require_once 'View/Page.php';
require_once 'View/User.php';
require_once 'Helper.php';
require_once 'Core/Routing/Router.php';
require_once 'App.php';