<?php

namespace Akira\Core;

/**
 * Site Class
 */
class Site
{
    /** @var array */
    protected array $siteData = [];

    /**
     * Load the sites json
     */
    public function __construct(string $domain = '')
    {
        // default to the server http if no domain is passed
        $domain = empty($domain) ? $_SERVER['HTTP_HOST'] : $domain;

        // TODO: this json should not be part of the repo long term.
        $sites = json_decode(
            file_get_contents('/var/akira/Core/Config/sites.json'),1
        );
        $this->siteData = $sites[$domain] ?? 0;
    }

    /**
     * Get the data for the current domain
     *
     * @return array
     */
    public function getSiteData(): array
    {
        return $this->siteData;
    }

    /**
     * Get the id for the current domain
     *
     * @return int
     */
    public function getSiteId(): int
    {
        return $this->getSiteData()['id'];
    }

    /**
     * Get the path for the current domain
     *
     * @return string
     */
    public function getSitePath(): string
    {
        return $this->getSiteData()['path'];
    }
}