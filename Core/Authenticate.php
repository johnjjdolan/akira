<?php

namespace Akira\Core;

use Akira\Handler\User;
use Exception;

/**
 * This class handles all admin Authentication
 */
class Authenticate
{
    /**
     * Check if the user is logged into the Session
     *
     * @return bool
     */
    public function isLoggedIn(): bool
    {
        return isset($_SESSION['user_login']);
    }

    /**
     * Login a user
     *
     * @param $login
     * @param $password
     * @return bool
     */
    public function login($login, $password): bool
    {
        try {
            $this->check($login, $password);
            $_SESSION['user_login'] = time();
            $_SESSION['user_name'] = $login;
            return true;
        } catch (Exception $e) {
            // do nothing, login failed
        }
        return false;
    }

    /**
     * Logout a user
     *
     * @return bool
     */
    public function logout(): bool
    {
        $loggedIn = $this->isLoggedIn();

        // always cleanup
        if (isset($_SESSION['user_login'])) {
            unset($_SESSION['user_login']);
        }
        if (isset($_SESSION['user_name'])) {
            unset($_SESSION['user_name']);
        }

        return $loggedIn;
    }

    /**
     * Lookup user and compare passwords
     *
     * @param string|null $login
     * @param string|null $password
     * @return bool
     * @throws Exception
     */
    public function check(string $login = null, string $password = null): bool
    {
        if (empty($password)) {
            throw new Exception('(!) Error: Please provide a password');
        }
        if (empty($login)) {
            throw new Exception('(!) Error: Please provide a login');
        }

        // find user
        $user = (new User())->getOne(['login' => $login]);
        if (!$user) {
            throw new Exception('(!) Error: User not found');
        }

        // compare password hashes
        if (!password_verify($password, $user->getPassword())) {
            throw new Exception('(!) Error: Incorrect password');
        }

        return true;
    }
}