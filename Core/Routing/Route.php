<?php

namespace Akira\Core\Routing;

use Akira\Controllers\DefaultController;

/**
 * Route class
 */
class Route
{
    /** @var string */
    protected string $method;
    /** @var DefaultController */
    protected DefaultController $controller;
    /** @var string|null */
    protected ?string $action;

    /**
     * @param string $method
     * @param string|null $action
     */
    public function __construct(string $method = 'GET', ?string $action = null)
    {
        $this->method = $method;
        $this->action = $action;
        $this->controller = new DefaultController();
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @param string $method
     * @return Route
     */
    public function setMethod(string $method): Route
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @return DefaultController|null
     */
    public function getController(): ?DefaultController
    {
        return $this->controller;
    }

    /**
     * @param DefaultController $controller
     * @return Route
     */
    public function setController(DefaultController $controller): Route
    {
        $this->controller = $controller;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAction(): ?string
    {
        return $this->action;
    }

    /**
     * @param string|null $action
     * @return Route
     */
    public function setAction(?string $action): Route
    {
        $this->action = $action;
        return $this;
    }
}