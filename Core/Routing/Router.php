<?php

namespace Akira\Core\Routing;

use Akira\Controllers\Authorise;
use Akira\Controllers\DefaultController;
use Akira\Handler\Page;
use Exception;

/**
 *
 */
class Router
{
    /** @var array */
    protected array $routes = [];
    /** @var string */
    protected string $routesPath = '/var/akira/Core/Routing/routes.json';

    /**
     * @param string $method
     * @param string $requestUrl
     * @param array $request
     * @throws Exception
     */
    public function handleRequest(string $method = 'GET', string $requestUrl = '', array $request = []): void
    {
        // is this a backend or a frontend request?
        $route = preg_match("/admin/i", $requestUrl) ?
            $this->adminRoute($method, $requestUrl, $request) :
            $this->siteRoute($method, $requestUrl);

        // add the route for the request
        $this->routes[] = $route;
    }

    /**
     * @param string $method
     * @param string $requestUrl
     * @param array $request
     * @return Route
     * @throws Exception
     */
    private function adminRoute(string $method = 'GET', string $requestUrl = '', array $request = []): Route
    {
        // if user is logged in, then build the route for the requested view
        $action = $request['action'] ?? null;
        $entity = $request['entity'] ?? null;
        $instanceId = $request['id'] ?? null;
        $mode = 'web';
        $authorise = new Authorise();

        // use the passed in action path
        if (!$authorise->isLoggedIn() && isset($request['action'])) {
            $requestUrl = $request['action'];
        }

        // create an array from the URL, remove "admin" as we don't need it
        $urls = array_filter(
            explode('/',
                preg_replace('/admin/','', $requestUrl)
            )
        );

        // figure out the entity, action and id from the URL
        if ($urls) {
            // loop all parts of the url
            foreach ($urls as $url) {
                // the first element is always the entity
                if (empty($entity)) {
                    $entity = $url;
                    continue;
                }
                // any numeric part is the id
                if (is_numeric($url)) {
                    $instanceId = $url;
                }
                // anything else is the action
                if (!is_numeric($url)) {
                    $action = $url;
                }
            }
        }
        $route = new Route($method, $action);

        // is user logged in?
        if (!$authorise->isLoggedIn() && $action != 'login') {
            $route->setController($authorise);
            $entity = null;
        }

        // lookup the controller
        if ($entity) {
            $routes = json_decode(file_get_contents($this->routesPath), 1);
            foreach ($routes[$method] as $routeEntity => $controller) {
                if ($routeEntity === $entity) {
                    $controllerClass = "Akira\Controllers\\" . $controller;
                    $route->setController(new $controllerClass((int)$instanceId, $mode));
                    break;
                }
            }
        }

        return $route;
    }

    /**
     * @param string $method
     * @param string $requestUrl
     * @return Route
     * @throws Exception
     */
    private function siteRoute(string $method, string $requestUrl): Route
    {
        // lookup the page based on the request (slug)
        $page = (new Page())->getOne(['slug' => str_replace('/', '', $requestUrl)]);
        $controller = new DefaultController();
        $route = new Route($method);
        if ($page) {
            $controller->addVariable('page', $page);
            $controller->setView($page->getTemplate());
        }
        if (!$page) {
            $controller->setView('/templates/404');
        }

        $route->setController($controller);
        return $route;
    }

    /**
     * @return Route
     */
    public function getRoute(): Route
    {
        return current($this->routes);
    }
}