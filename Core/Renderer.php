<?php

namespace Akira\Core;

use Akira\Helper;
use Smarty;

class Renderer
{
    /**
     * @param string $view
     * @param array $variables
     * @return mixed
     */
    public function render(string $view, array $variables = [])
    {
        $renderer = $this->getTemplateEngine();

        // assign the variables to the template
        $renderer->assign('view', $view);
        $renderer->assign('akira', new Helper());
        $renderer->assign('login', $_SESSION['user_name'] ?? null);

        // always assign the messages variable
        if (!isset($variables['messages'])) {
            $variables['messages'] = [];
        }
        foreach ($variables as $name => $variable) {
            if ($name == 'messages') {
                $variable = $this->formatMessages($variables['messages']);
            }
            $renderer->assign($name, $variable);
        }

        // render template content (& remove .html if passed in already)
        return $renderer->display(
            (new Site())->getSitePath() . str_replace('.html', '', $view) . '.html'
        );
    }

    /**
     * @return Smarty
     */
    private function getTemplateEngine(): Smarty
    {
        $smarty = new Smarty();
        $smarty->setTemplateDir('/var/sites/admin/smarty/templates');
        $smarty->setCompileDir('/var/sites/admin/smarty/templates_c');
        $smarty->setCacheDir('/var/sites/admin/smarty/cache');
        $smarty->setConfigDir('/var/sites/admin/smarty/configs');
        // controlled by config settings
        $smarty->setForceCompile(true);
        return $smarty;
    }

    /**
     * Build the messages array and ensure we have the same format everytime
     *
     * @param array $messages
     * @return array
     */
    private function formatMessages(array $messages = []): array
    {
        return [
            'success' => $messages['success'] ?? [],
            'errors' => $messages['errors'] ?? [],
            'warnings' => []
        ];
    }
}