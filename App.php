<?php

namespace Akira;

use Akira\Core\Routing\Router;
use Exception;

// TODO: Implement autoloader

/**
 * Akira App
 */
class App
{
    /**
     * Kick off the Akira CMS logic
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        // start the user session
        session_start();
        $router = new Router();

        // dispatch the request
        $router->handleRequest(
            $_SERVER['REQUEST_METHOD'],
            $_SERVER['REQUEST_URI'],
            $_REQUEST
        );

        // get the route
        $route = $router->getRoute();
        $controller = $route->getController();
        $action = $route->getAction();

        // perform the action
        if ($action && method_exists($controller, $action)) {
            $controller->$action();
        }

        // return content
        return $controller->display();
    }
}