<?php

namespace Akira;

use Akira\Handler\Page;
use Akira\Handler\User;

/**
 * This is the object assigned to the template to help fetch objects
 */
class Helper
{
    /**
     * @return int
     */
    public function getSiteId(): int
    {
        return 1;
    }

    /**
     * @return Handler\Page
     */
    public function getPagesHandler(): Handler\Page
    {
        return new Page();
    }

    /**
     * @return User
     */
    public function getUsersHandler(): Handler\User
    {
        return new User();
    }

    /**
     * @param $name
     * @return Handler\BaseHandler
     */
    public function getHandler($name): Handler\BaseHandler
    {
        $class = "\Akira\Handler\\" . $name;
        return new $class();
    }

    public function getUser()
    {
        return (new User())->getOne(['login' => $_SESSION['user_name']]);
    }
}