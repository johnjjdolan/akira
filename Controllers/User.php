<?php

namespace Akira\Controllers;

/**
 * User Controller
 */
class User extends DefaultController
{
    /**
     * Get the Default view
     *
     * @return string
     */
    public function getDefaultView(): string
    {
        return 'admin/templates/users.html';
    }

    /**
     * Show a new User form
     *
     * @return void
     */
    public function new()
    {
        // set the view
        $this->setView('admin/templates/entity.html');
        // pass the user entity object
        $this->addVariable('entity', new \Akira\Entity\User());
    }

    /**
     * Show an edit User form
     *
     * @return void
     */
    public function edit()
    {
        // set the view
        $this->setView('admin/templates/entity');
        $user = (new \Akira\Handler\User())->getOne(['id' => $this->getInstanceId()]);
        // pass the user entity object
        $this->addVariable('entity', $user);
    }

    /**
     * Update a User
     *
     * @param array $data
     * @return bool
     */
    public function save(array $data = []): bool
    {
        if (empty($data)) {
            $data = $_REQUEST;
        }
        if (empty($data['id'])) {
            $data['id'] = $this->getInstanceId();
        }
        $result = (new \Akira\Model\User())->save($data);
        $result ? $this->addSuccessMsg("User Saved") : $this->addGenericErrorMsg();
        return $result;
    }

    /**
     * Delete a User
     *
     * @param array $data
     * @return bool
     */
    public function delete(array $data = []): bool
    {
        if (empty($data)) {
            $data = $_REQUEST;
        }
        $id = $data['id'] ?? $this->getInstanceId();
        $user = (new \Akira\Handler\User())->getOne(['id' => $id]);
        $result = (new \Akira\Model\User())->delete([
            'item_id' => $id,
        ]);
        $result ? $this->addSuccessMsg("User '{$user->getName()}' Deleted") : $this->addGenericErrorMsg();
        return $result;
    }
}