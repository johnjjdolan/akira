<?php

namespace Akira\Controllers;

/**
 * Page Controller
 */
class Page extends DefaultController
{
    /**
     * Get the Default view
     *
     * @return string
     */
    public function getDefaultView(): string
    {
        return 'admin/templates/pages.html';
    }

    /**
     * Show a new Page form
     *
     * @return void
     */
    public function new()
    {
        // set the view
        $this->setView('admin/templates/entity');
        // pass the page entity object
        $this->addVariable('entity', new \Akira\Entity\Page());
    }

    /**
     * Show an edit Page form
     *
     * @return void
     */
    public function edit()
    {
        // set the view
        $this->setView('admin/templates/entity');
        $page = (new \Akira\Handler\Page())->getOne(['id' => $this->getInstanceId()]);
        // pass the page entity object
        $this->addVariable('entity', $page);
    }

    /**
     * Update a Page
     *
     * @param array $data
     * @return bool
     */
    public function save(array $data = []): bool
    {
        if (empty($data)) {
            $data = $_REQUEST;
        }
        if (empty($data['id'])) {
            $data['id'] = $this->getInstanceId();
        }
        $result = (new \Akira\Model\Page())->save($data);
        $result ? $this->addSuccessMsg("Page Saved") : $this->addGenericErrorMsg();
        return $result;
    }

    /**
     * Delete a Page
     *
     * @param array $data
     * @return bool
     */
    public function delete(array $data = []): bool
    {
        if (empty($data)) {
            $data = $_REQUEST;
        }
        $id = $data['id'] ?? $this->getInstanceId();
        $page = (new \Akira\Handler\Page())->getOne(['id' => $id]);
        $result = (new \Akira\Model\Page())->delete([
            'item_id' => $id,
        ]);
        $result ? $this->addSuccessMsg("Page '{$page->getName()}' Deleted") : $this->addGenericErrorMsg();
        return $result;
    }
}