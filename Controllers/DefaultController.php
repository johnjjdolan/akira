<?php

namespace Akira\Controllers;

use Akira\Core\Renderer;
use Akira\Model\ControllerInterface;

/**
 * Default Controller
 */
class DefaultController implements ControllerInterface
{
    /** @var string  */
    protected string $mode;
    /** @var array */
    protected array $variables = [];
    /** @var string */
    protected string $view = '';
    /** @var int|null */
    protected ?int $instanceId = null;

    /**
     * @param null $instanceId
     * @param string $mode
     */
    public function __construct($instanceId = null, string $mode = 'web')
    {
        $this->instanceId = $instanceId;
        $this->mode = $mode;
    }

    /**
     * @return mixed
     */
    public function display()
    {
        // TODO: are we in JSON, REST or HTTP?
        return (new Renderer())->render(
            !empty($this->getView()) ? $this->getView() : $this->getDefaultView(),
            $this->getVariables(),
        );
    }

    /**
     * @param $msg
     * @param string $type
     * @return void
     */
    private function addMessage($msg, string $type = 'warning'): void
    {
        $this->variables['messages'][$type][] = $msg;
    }

    /**
     * @param $msg
     * @return DefaultController
     */
    public function addErrorMsg($msg): DefaultController
    {
        $this->addMessage($msg, 'errors');
        return $this;
    }

    /**
     * @return DefaultController
     */
    public function addGenericErrorMsg(): DefaultController
    {
        $this->addMessage('There was a problem, Please try again', 'errors');
        return $this;
    }

    /**
     * @param $msg
     * @return DefaultController
     */
    public function addSuccessMsg($msg): DefaultController
    {
        $this->addMessage($msg, 'success');
        return $this;
    }

    /**
     * @return string
     */
    public function getMode(): string
    {
        return $this->mode;
    }

    /**
     * @return array
     */
    public function getVariables(): array
    {
        return $this->variables;
    }

    /**
     * @param $name
     * @param $var
     * @return $this
     */
    public function addVariable($name, $var): DefaultController
    {
        $this->variables[$name] = $var;
        return $this;
    }

    /**
     * @param $type
     * @return array
     */
    public function getMessageType($type): array
    {
        return $this->messages[$type] ?? [];
    }

    /**
     * @return string
     */
    public function getView(): string
    {
        return $this->view;
    }

    /**
     * @param string $view
     * @return DefaultController
     */
    public function setView(string $view): DefaultController
    {
        $this->view = $view;
        return $this;
    }

    /**
     * @return string
     */
    public function getDefaultView(): string
    {
        return 'admin/templates/dashboard.html';
    }

    /**
     * @return int|null
     */
    public function getInstanceId(): ?int
    {
        return $this->instanceId;
    }

    /**
     * @param int|null $instanceId
     * @return DefaultController
     */
    public function setInstanceId(?int $instanceId): DefaultController
    {
        $this->instanceId = $instanceId;
        return $this;
    }

    /**
     * @return void
     */
    public function new()
    {}

    /**
     * @return void
     */
    public function edit()
    {}

    /**
     * @param array $data
     * @return bool
     */
    public function save(array $data = []): bool
    {
        return true;
    }

    /**
     * @param array $data
     * @return bool
     */
    public function delete(array $data = []): bool
    {
        return true;
    }
}