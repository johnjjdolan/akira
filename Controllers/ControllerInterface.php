<?php

namespace Akira\Model;

/**
 * Controller Interface
 */
interface ControllerInterface
{
    /**
     * @return void
     */
    public function new();

    /**
     * @return void
     */
    public function edit();

    /**
     * @param array $data
     * @return bool
     */
    public function save(array $data = []): bool;

    /**
     * @param array $data
     * @return bool
     */
    public function delete(array $data = []): bool;
}