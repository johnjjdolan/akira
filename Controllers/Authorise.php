<?php

namespace Akira\Controllers;

use Akira\Core\Authenticate;
use Exception;

/**
 * Authorise Controller
 */
class Authorise extends DefaultController
{
    /** @var Authenticate */
    protected Authenticate $authenticate;

    /**
     *
     */
    public function __construct()
    {
        $this->authenticate = new Authenticate();
    }

    /**
     * Log a user in
     *
     * @return bool
     */
    public function login(): bool
    {
        $login = $_REQUEST['login'] ?? null;
        $password = $_REQUEST['password'] ?? null;
        $result = $this->authenticate->login($login, $password);
        if (!$result) {
            $this->addErrorMsg('Invalid Login');
        }
        if ($result) {
            $this->addSuccessMsg('Logged In');
            $this->setView('admin/templates/dashboard');
        }
        return $result;
    }

    /**
     * Log a user out
     *
     * @return bool
     */
    public function logout(): bool
    {
        $result = $this->authenticate->logout();
        if (!$result) {
            $this->addErrorMsg('There was an error logging out');
            $this->setView('admin/templates/dashboard');
        }
        if ($result) {
            header('location: /admin/');
        }
        return $result;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function isLoggedIn(): bool
    {
        return $this->authenticate->isLoggedIn();
    }

    /**
     * @return string
     */
    public function getDefaultView(): string
    {
        return 'admin/templates/login.html';
    }
}